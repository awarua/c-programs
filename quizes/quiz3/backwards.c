/* Simple array demo program.
 * Also demonstrates sizeof and the use of getchar/putchar.
 * Reads a line of input characters into a 1D array
 * of 'char', then writes the line out backwards.
 * Written for ENCE260 by RJL.
 * February 2011, June 2013.
 */

#include <stdlib.h>
#include <stdio.h>

#define N_MAX 1

int main() {
    
    char line[N_MAX] = {'\0'};  // An array of char, init'ed to zero
    int c = 0;                  // An int with a char in its low byte
    int n = 0;
    
    printf("Variable n requires %ld bytes of memory\n", sizeof n);
    printf("Array line occupies %ld bytes of memory\n", sizeof line);
    
    printf("Enter a line of text, terminated by 'Enter'\n");
    
    // Read characters until EOF, newline or buffer full
    
    c = getchar();  /* Get char (cast to int) or EOF */
    while (c != EOF && c != '\n' && n < N_MAX) {
        line[n] = c;
        n += 1;
        c = getchar();
    }
    
    // Now print out all those characters backwards
    
    printf("Your input line, written backwards, is:\n");
    for (int i = n - 1; i >= 0; i--) {
        putchar(line[i]);
    }
    putchar('\n');
    
    // Lastly print the characters in various different ways
    
    puts("The numeric representations of those characters are:");
    puts(" i  c dec oct hex");
    for (int i = 0; i < n; i++) {
        c = line[i];
        printf("%2d%3c%4d%4o%4x\n", i, c, c, c, c);
    }
    putchar('\n');
        
    return EXIT_SUCCESS;
}
