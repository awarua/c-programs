/***********************************************************************
 miles_to_kilometers.c 
 Converts a given distance in miles to kilometers
 Author Dion Woolley
 **********************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define SCALE_FACTOR 1.609344

int main(){
	float mile = 0, kilometer =0;
	printf("How many miles? ");
	scanf("%f", &mile);
	kilometer = mile * SCALE_FACTOR;
	printf("That's %.2f km.\n", kilometer);
	return EXIT_SUCCESS;
}
