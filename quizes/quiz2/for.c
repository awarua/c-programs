#include <stdio.h>
#include <stdlib.h>
int main() {
	int count = 0;
	int total = 0;
	float avg = 0;
	int current = 0;
	
	while (1){
		if (scanf("%d\n", &current) != 1){
			break;
		}
		else{
			count++;
			total += current;
			avg = (avg + current) / count;
		}
	}
	if (count != 0){
		printf("Count: %d\nTotal: %d\nAverage: %.2f", count, total, avg);
	}
	else{
		printf("Count: %d\nTotal: %d", count, total);
	}
	
	return(EXIT_SUCCESS);	
}
