#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
int main() {
	int current = 0;
	int counts[26] = {0};
	current = getchar();
	while (current != -1){
		if (isalpha(current)) {
			current = toupper(current);
			counts[current - 65] += 1;
		}
		current = getchar();
	}
	for (int i = 0; i < 26; i++) {
		printf("%c: %d\n", i + 65, counts[i]);
	}
	return(EXIT_SUCCESS);	
}
