#include <stdio.h>
#include <malloc.h>
#include <string.h>
 
/*
 * prints the frequency of words in a given text, with a given threshold
 * 
 * Dion Woolley
 * 13/08/2014
 * 
 */

#define MAX_WORD_SIZE 21 //20 +1 for the null terminator


typedef struct BST_s BST;


//Struct for a node on a Binary Search Tree 
struct BST_s {
    char* word;
    int count;
    BST* left;
    BST* right;
};


BST* newNode(const char* word);
int compareWords(const char* word1, const char* word2);
BST* checkIfWordExists(char* word, BST* head);
BST* readOneWord(FILE* file, BST* head);
BST* readWords(FILE* file);
void printBSTRec(BST* node, int threshold);
void printBST(BST* tree, int threshold);
void myprintBST(BST* tree, int threshold);
void printSingleWord(BST* node, int threshold);
void processInput(int threshold, const char* filename);
void destroyNode(BST* node);


//Creates a new node
BST* newNode(const char* word) {
    BST* node = NULL;
    int wordSize = 0;
    node = malloc(sizeof(BST));
    if (node != NULL) {
		wordSize = strlen(word);
		node->word = malloc(wordSize + 1);
		if (node->word == NULL) {
			free(node);
			node = NULL;
		}
		else {
			strncpy(node->word, word, wordSize + 1);
			node->count = 1;
			node->left = NULL;
			node->right = NULL;
		}
	}
    return node;
}


//Destorys a node to free memory
void destoryNode(BST* node) {
	free(node->word);
	free(node);
}
	
	
//Compares two words to see if they are large smaller of match one another
int compareWords(const char* word1, const char* word2) {
	int cmpValue = strcmp(word1, word2);
	return  cmpValue >= 0 ? cmpValue == 0 ? 0 : -1 : 1;
}	


//Checks to see if the word exists in the BST if it does it increments
//the count of that node other wise it inserts a new node in the appropiate
//place in the BST
BST* checkIfWordExists(char* word, BST* head) {
	if (head == NULL) {
		head = newNode(word);
		return head;
	}
	else {
		BST* currentVisiting = head;
		while (compareWords(currentVisiting->word, word) != 0) {      
			// not same
			if (compareWords(currentVisiting->word, word) == 1) {
				// go right
				if (currentVisiting->right != NULL) {
					currentVisiting = currentVisiting->right;
				}
				else {
					BST* node = newNode(word);
					//if hit bottom of tree create new node and assign pointers
					currentVisiting->right = node;
					return head;
				}
			}
			else{
				if (currentVisiting->left != NULL) { 
					currentVisiting = currentVisiting->left; //else left
				}
				else {
					BST* node = newNode(word);
					currentVisiting->left = node;
					return head;
				}
			}	
		}
		currentVisiting->count++;         //same incerment count
		return head;
	}
}		

	
//Reads one word from the text
BST* readOneWord(FILE* file, BST* head) {
    char buffer[MAX_WORD_SIZE];
    BST* node = NULL;
    char* cp = fgets(buffer, MAX_WORD_SIZE, file);
    buffer[MAX_WORD_SIZE - 1] = '\0';
    if (cp != NULL) {
		char* newlinePos = strchr(buffer, '\n');
		if (newlinePos != NULL) { 
			*newlinePos = '\0';
		}
		node = checkIfWordExists(buffer, head);
    }
    return node;
}


//Builds the BST 
BST* readWords(FILE* file) {	
	BST* initial = NULL;
    BST* head = readOneWord(file, initial);
    BST* node = head;
    while (node != NULL) {
        node = readOneWord(file, head);
    }
    return head;
}


//prints the BST using recursion
void printBSTRec(BST* node, int threshold) {
	if (node->left != NULL) {
		printBSTRec(node->left, threshold);
	}
	printSingleWord(node, threshold);	
	if (node->right != NULL) {
		printBSTRec(node->right, threshold);
	}
	destoryNode(node);
	return;
}


//prints the node
void printSingleWord(BST* node, int threshold) {
	if (node->count >= threshold) {
		printf("%s: %d\n", node->word, node->count);
	}
}


//process the input either from a file or stdin
void processInput(int threshold, const char* filename) {
	
	if (filename != NULL){
		FILE* inputFile = fopen(filename, "r");
		if (inputFile == NULL) {
			printf("File %s does not exist\n", filename);
		}
		else {
		    BST* tree = readWords(inputFile);
		    if (tree != NULL) {
				printBSTRec(tree, threshold);
			}	
		}	
		fclose(inputFile);
	}
    else {
		FILE* inputFile = stdin;
		if (inputFile == NULL) {
			printf("You didn't give an input\n");
		}
		else {
			BST* tree = readWords(inputFile);
			if (tree != NULL) {
				printBSTRec(tree, threshold);
			}
		}
		fclose(inputFile);
	}
}


int main() {
    int allocatedBlocks = 0;
    int leakedBlocks = 0;

    allocatedBlocks = mallinfo().uordblks + mallinfo().hblkhd;
    processInput(1, NULL);
    leakedBlocks = mallinfo().uordblks + mallinfo().hblkhd - allocatedBlocks;
    if (leakedBlocks != 0) {
        printf("Memory leak of %d bytes detected!\n", leakedBlocks);
    }
    
    return 0;
}
