

#include <stdio.h>

int main(){

	int number;
	number = 0;
	
	scanf("%d", &number);
	
	if (number == 0)
		printf("Zero");
	else if (number % 2 == 0)
		printf("Even");
	else
		printf("Odd");
	
	
	return 0;
}
