/* 
def main():
* 	course = 260
	print("Hello {}".format(course))

main()
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

int main(void){
	char c = 'x';
	bool thing = true;
	if (thing){
		int course = 0;
		course = 260;
		printf("Hello %10d\n", course);
		printf("%c\n", c);
		printf("%d\n", c);
		return EXIT_SUCCESS;
	}
}
