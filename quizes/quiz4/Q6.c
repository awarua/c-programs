#include <stdio.h>
#include <stdlib.h>

void findTwoLargest(const int a[], int n, int* largest, int* secondLargest) {
	
	if (a[0] >= a[1]) {
	*largest = a[0];
	*secondLargest = a[1];	
	}
	else {
		*largest = a[1];
		*secondLargest = a[0];
	}

	n--;
	while (n > 1) {
		if (a[n] > *secondLargest) {
			if (a[n] > *largest) {
				*secondLargest = *largest;
				*largest = a[n];
			}
			else {
				*secondLargest = a[n];
			}
		}
		n--;
	}
}

int main() {
	
	int data[] = {1, 3, 4, 0, 5, 3, 2, -1};
	int result1 = 0, result2 = 0;
	findTwoLargest(data, 8, &result1, &result2);
	printf("%d %d\n", result1, result2);
	
	int data2[] = {5, 4};
	int result12 = 0, result22 = 0;
	findTwoLargest(data2, 2, &result12, &result22);
	printf("%d %d\n", result12, result22);
	printf("%d %d\n", data2[0], data2[1]);
}
