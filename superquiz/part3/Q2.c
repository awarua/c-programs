#include <stdio.h>
#include <malloc.h>
#include <string.h>
 
/*
 * prints the frequency of words in a given text, with a given threshold
 * 
 */

#define MAX_WORD_SIZE 21

#define MAX_NUM_WORDS 717038


typedef struct BST_s BST;


//Struct for a node on a Binary Search Tree 
struct BST_s {
    char word[MAX_WORD_SIZE];
    int count;
    int visited;
    BST* parent;
    BST* left;
    BST* right;
};


//Pool of nodes to uses
BST nodePool[MAX_NUM_WORDS];
int firstFree = 0;


BST* newNode(const char* word);
int compareWords(const char* word1, const char* word2);
BST* checkIfWordExists(char* word, BST* head);
BST* readOneWord(FILE* file, BST* head);
BST* readWords(FILE* file);
void printBSTRec(BST* node, int threshold);
void printBST(BST* tree, int threshold);
void myprintBST(BST* tree, int threshold);
void printSingleWord(BST* node, int threshold);
void processInput(int threshold, const char* filename);


//Creates a new node
BST* newNode(const char* word) {
    BST* node = NULL;
    if (firstFree < MAX_NUM_WORDS) {
        node = &nodePool[firstFree];
        firstFree += 1;
        strncpy(node->word, word, MAX_WORD_SIZE);
        node->word[MAX_WORD_SIZE - 1] = '\0';
        node->count = 1;
        node->visited = 0;
    }
    return node;
}


//Compares two words to see if they are large smaller of match one another
int compareWords(const char* word1, const char* word2) {
	int cmpValue = strcmp(word1, word2);
	return  cmpValue >= 0 ? cmpValue == 0 ? 0 : -1 : 1;
}	


//Checks to see if the word exists in the BST if it does it increments
//the count of that node other wise it inserts a new node in the appropiate
//place in the BST
BST* checkIfWordExists(char* word, BST* head) {
	if (head == NULL) {
		head = newNode(word);
		return head;
	}
	else {
		BST* currentVisiting = head;
		while (compareWords(currentVisiting->word, word) != 0) {      
			// not same
			if (compareWords(currentVisiting->word, word) == 1) {                        
				// go right
				if (currentVisiting->right != NULL) {
					currentVisiting = currentVisiting->right;
				}
				else {
					BST* node = newNode(word);					
					//if hit bottom of tree create new node and assign pointers
					node->parent = currentVisiting;
					currentVisiting->right = node;
					return head;
				}
			}
			else{
				if (currentVisiting->left != NULL) { 
					currentVisiting = currentVisiting->left;						
					//else left
				}
				else {
					BST* node = newNode(word);
					node->parent = currentVisiting;
					currentVisiting->left = node;
					return head;
				}
			}	
		}
		currentVisiting->count++;         //same incerment count
		return head;
	}
}		

	
//Reads one word from the text
BST* readOneWord(FILE* file, BST* head) {
    char buffer[MAX_WORD_SIZE];
    BST* node = NULL;
    char* cp = fgets(buffer, MAX_WORD_SIZE, file);
    buffer[MAX_WORD_SIZE - 1] = '\0';
    if (cp != NULL) {
		char* newlinePos = strchr(buffer, '\n');
		if (newlinePos != NULL) { 
			*newlinePos = '\0';
		}
		node = checkIfWordExists(buffer, head);
    }
    return node;
}



//Builds the BST 
BST* readWords(FILE* file) {	
	BST* initial = NULL;
    BST* head = readOneWord(file, initial);
    BST* node = head;
    while (node != NULL) {
        node = readOneWord(file, head);
    }
    return head;
}


//prints the BST using recursion
void printBSTRec(BST* node, int threshold) {
	if (node->left != NULL) {
		printBSTRec(node->left, threshold);
	}
	printSingleWord(node, threshold);	
	if (node->right != NULL) {
		printBSTRec(node->right, threshold);
	}
	return;
}


//prints the BST in order using Morris. 
void printBST(BST* tree, int threshold) {
	
	if (tree == NULL)
		return; 
	BST* pre = NULL;
	BST* current = tree;

	while (current != NULL) {
		if (current->left == NULL) {
			printSingleWord(current, threshold);
			current = current->right;
		}
		else {
			//Find the inorder predecessor of current
			pre = current->left;
			while (pre->right != NULL && pre->right != current)
			pre = pre->right;

			//Make current as right child of its inorder predecessor
			if (pre->right == NULL) {
				pre->right = current;
				current = current->left;
			}
			// Revert the changes made in if part to restore the original tree
			else {
				pre->right = NULL;
				printSingleWord(current, threshold);
				current = current->right;
			} 
		}
	}
}


//prints the node
void printSingleWord(BST* node, int threshold) {
	if (node->count >= threshold) {
		printf("%s: %d\n", node->word, node->count);
	}
}


//process the input either from a file or stdin
void processInput(int threshold, const char* filename) {
	
	if (filename != NULL){
		FILE* inputFile = fopen(filename, "r");
		if (inputFile == NULL) {
			printf("File %s does not exist\n", filename);
		}
		else {
		    BST* tree = readWords(inputFile);
			printBSTRec(tree, threshold);
			fclose(inputFile);	
		}	
	}
    else {
		FILE* inputFile = stdin;
		BST* tree = readWords(inputFile);
		printBSTRec(tree, threshold);
		fclose(stdin);
	}
}



int main() {
    int allocatedBlocks = 0;
    int leakedBlocks = 0;

    allocatedBlocks = mallinfo().uordblks + mallinfo().hblkhd;
    printf("Allocated blocks %d\n", allocatedBlocks);
    processInput(1, NULL);
    leakedBlocks = mallinfo().uordblks + mallinfo().hblkhd - allocatedBlocks;
    if (leakedBlocks != 0) {
        printf("Memory leak of %d bytes detected!\n", leakedBlocks);
    }
    printf("Leaked blocks %d\n", leakedBlocks);
    
    return 0;
}
