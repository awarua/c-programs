// Some examples of the uses of string literals
#include <stdio.h>

int tokenCopy(char* dest, const char* src, int destSize){
	
	int i = 0;
	while (src[i] != 0 && src[i] != ' ' && i < destSize - 1) {
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	
	return(i);
}


int main(void)
{
	char buff[5];
	int n = tokenCopy(buff, "This is a string", 5);
	printf("%d '%s'\n", n, buff);
	
	char buff2[3];
	int n2 = tokenCopy(buff2, "This is a string", 3);
	printf("%d '%s'\n", n2, buff2);
	
	char buff3[7];
	char name[] = {'D', 'i', 'o', 'n', '\0'};
	int n3 = tokenCopy(buff3, name, 7);
	printf("%d '%s'\n", n3, buff3);
	
}
