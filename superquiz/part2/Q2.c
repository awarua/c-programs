/*
 * 
 * Author: Dion Woolley
 * 
 * Date: 08/08/2014
 */


#include <stdio.h>



//Caesar cipher shift function, takes a string
//and shifts by a given amount.
//ignores all non alpha charaters.
//keeps case of alpha characters.
void confab(const char* inText, int shift, char* outText) {
	
	int i = 0;
	
	while (inText[i] != '\0') {
		int newChar = inText[i] + shift;
		if (inText[i] <= 'z' && inText[i] >= 'a') {
			if((newChar - 'a') >= 26) {
				newChar -= 26;
			}
			else if ((newChar - 'a') < 0) {
				newChar += 26;
			}
		}
		else if (inText[i] <= 'Z' && inText[i] >= 'A') {
			if((newChar - 'A') >= 26) {
				newChar -= 26;
			}
			else if ((newChar - 'A') < 0) {
				newChar += 26;
			}
		}
		else {
			newChar = inText[i];	
		}  
		outText[i] = newChar;
		i++;
	}
	outText[i] = '\0';
}


int main(void)
{
	char buffer1[40] = {'\0'};
	confab("ALL HAIL CAESAR", 2, buffer1);
	printf("%s\n", buffer1);

	char buffer2[40] = {'\0'};
	confab("cnn jckn Ecguct!", -2, buffer2);
	printf("%s\n", buffer2);
	
	char buffer3[40] = {'\0'};
	confab("aaaaaa", 1, buffer3);
	printf("%s\n", buffer3);
	
	char buffer4[40] = {'\0'};
	confab("AAAA", 1, buffer4);
	printf("%s\n", buffer4);
	
	char buffer5[40] = {'\0'};
	confab("zzzzz", 1, buffer5);
	printf("%s\n", buffer5);
	
	char buffer6[40] = {'\0'};
	confab("ZZZZZ", 1, buffer6);
	printf("%s\n", buffer6);
	
	char buffer7[40] = {'\0'};
	confab("    !#@($@*%^!$*!128347", 5, buffer7);
	printf("%s\n", buffer7);
	
	char buffer8[4] = {'x','x','x','x'}; // Not zero!
	confab("XyZ", 13, buffer8);
	printf("%s\n", buffer8);
	
	char buffer9[1] = {'x'};
	confab("", 13, buffer9); // Empty string
	printf("%s\n", buffer9);
	
	char buffer10[4] = {'x','x','x','x'};
	confab("123", 13, buffer10); // Empty string
	printf("%s\n", buffer10);
	
	return 0;
}

