#include <math.h>

double discriminate(double a, double b, double c) {
	return sqrt(b * b - 4 * a * c);
}
