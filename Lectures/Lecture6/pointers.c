#include <stdio.h>

void printstuff(int n, const int* data) {
	const int* p= data;
	while (p < data + n) {
		printf("%d\n", *p);
		p++;
	}
}

int main(void) {
	int data[] = {1, 13,-99, 6};
	printstuff(4, data);
	printstuff(4, data);
}
