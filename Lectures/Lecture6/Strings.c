#include <stdio.h>
#include <string.h>

int len(const char* s) {
	int n = 0;
	while (s[n] != 0) {
		n++;
	}
	return n;
}

int main(void) {
	char name[] = {'D', 'i', 'o', 'n', '\0'};
	char* name2 = "Dion";
	printf("Length of %s = %ld\n", name, strlen(name));
	printf("Length of %s = %ld\n", name2, strlen(name2));
}
