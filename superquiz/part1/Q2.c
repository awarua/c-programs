#include <stdio.h>
#include <stdlib.h>
int main() {
	int store = 0;
	int current = 0;
	int sum  = 0;
	
	if (scanf("%d\n", &store) != 1){
		return(EXIT_SUCCESS);
	}

	while (scanf("%d\n", &current) == 1){
		if (current == store){
			printf("%d\n", sum);
			sum = 0;
			scanf("%d\n", &store);
		}
		else{
			sum += current;
		}
	}
	return(EXIT_SUCCESS);
}
