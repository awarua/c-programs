

#include <stdio.h>
#include <math.h>

int main(){

	float a, b, c, x1, x2;
	float det;
	a = b = c = det = x1 = x2 = 0;
	
	scanf("%f %f %f", &a, &b, &c);
	
	if (a == 0){
		printf("Not a quadratic");
		return 0;
	}
	
	det = (b * b) - (4 * a * c);
	
	if (det < 0){
		printf("Imaginary roots");
		return 0;
	}
		
	x1 = (- b + sqrt(det)) / (2 * a);
	x2 = (- b - sqrt(det)) / (2 * a);
	
	printf("Roots are %.4f and %.4f", x1 < x2 ? x1 : x2, x1 > x2 ? x1 : x2);
	
	return 0;
}
