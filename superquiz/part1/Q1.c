#include <stdio.h>
#include <stdlib.h>
int main() {
	int count = 0;
	float total = 0;
	float avg = 0;
	int current = 0;
	
	while (scanf("%d\n", &current) == 1){
		count++;
		total += current;
	}
	printf("Count: %d\nTotal: %.0f\n", count, total);
	if (count != 0){
		avg = total / count;
		printf("Average: %.2f\n", avg);
	}
	return(EXIT_SUCCESS);	
}
