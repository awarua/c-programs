#include <stdio.h>
#include <stdlib.h>


int main(void){

	int data[] = {10, 20, 30, 10000, -1, 2342, -10000};
	printf("Size of the test data = %ld\n", sizeof(data) / sizeof(int));
	printf("%d\n", data[5]);
	printf("%d\n", 5[data]);
	printf("%d\n", *(data + 5));
	printf("%d\n", *(5 + data));
	
	return(EXIT_SUCCESS);
}
