/*
 * 
 * Author: Dion Woolley
 * Date: 08/08/2014
 * Program shifts a text by 13 characters
 */


#include <stdio.h>
#include <string.h>

#define BUFFERSIZE 81
#define TEXTSIZE 81

//Caesar cipher shift function, takes a string
//and shifts by a given amount.
//ignores all non alpha charaters.
//keeps case of alpha characters.
void confab(const char* inText, int shift, char* outText) {
	
	int i = 0;
	
	while (inText[i] != '\0') {
		int newChar = inText[i] + shift;
		if (inText[i] <= 'z' && inText[i] >= 'a') {
			if((newChar - 'a') >= 26) {
				newChar -= 26;
			}
			else if ((newChar - 'a') < 0) {
				newChar += 26;
			}
		}
		else if (inText[i] <= 'Z' && inText[i] >= 'A') {
			if((newChar - 'A') >= 26) {
				newChar -= 26;
			}
			else if ((newChar - 'A') < 0) {
				newChar += 26;
			}
		}
		else {
			newChar = inText[i];	
		}  
		outText[i] = newChar;
		i++;
	}
	outText[i] = '\0';
}


int main(void)
{

	char buffer[BUFFERSIZE] = {'\0'};
	char text[TEXTSIZE] = {'\0'};
	
	while(fgets(buffer, BUFFERSIZE - 1, stdin) != NULL) {
		confab(buffer, 13, text);
		printf("%s", text);
	}	
	return 0;
}

