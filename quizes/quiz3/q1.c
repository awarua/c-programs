/* Simple array demo program.
 * Also demonstrates sizeof and the use of getchar/putchar.
 * Reads a line of input characters into a 1D array
 * of 'char', then writes the line out backwards.
 * Written for ENCE260 by RJL.
 * February 2011, June 2013.
 */

#include <stdlib.h>
#include <stdio.h>

#define N_MAX 100

int main() {
    
	int val = 0;
	printf("%ld\n", sizeof(val));
        
    return EXIT_SUCCESS;
}
