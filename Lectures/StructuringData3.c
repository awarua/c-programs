#include <stdio.h>
#include <string.h>

typedef struct pointStruct Point;

struct pointStruct { 
	int x;
	int y;
	Point* next;
};


int main() {


	Point p1 = {10, 20, NULL};
	Point p2 = {100, -30, NULL};
	p1.next = &p2;
	
	Point* p = &p1;
	while (p != NULL) {
		printf("p1 = (%d, %d)\n", p->x, p->y);
		p = p->next; 
	}
} 
