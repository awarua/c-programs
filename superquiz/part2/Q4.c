/*
 * 
 * Author: Dion Woolley
 * Date: 08/08/2014
 * Finds the path from the emerald city to dorathy.
 * Takes an input of row colnum.
 * then the map itself.
 * 
 * Starts at the Emerald City E and works backwards towards D.
 * Returns the count of the path between E and D.
 * If no path is found then returns the count = 0.
 * 
 */


#include <stdio.h>
#include <string.h>

#define MAXMAPBOUNDS 100
#define VALUESIZE 2
#define BUFFERSIZE 2

typedef enum {ONE, ONEN, ZERO, NONE} offSet;
int offSets[] = {1, -1, 0, 999};

typedef struct mapPoint_s mapPoint;


//Struct for each map point.
//Row stores row in map.
//Colnum stores colnum in map.
//value stores the value of the node on the map either . Y D E
//visited if 0 then the node has not been visted else 1 if it has.
//mapPointers up, down, left, right points to other points.
//that are adjacent to the point.
struct mapPoint_s {
	int row;
	int colnum;
	char value[VALUESIZE];
	int visited;
	mapPoint* up;
	mapPoint* down;
	mapPoint* left;
	mapPoint* right;
};


//Gets the row and colnums form stdin.
void getRowsColnums(int* row, int* colnum) {
	scanf("%d %d", row, colnum);
}


//Prints the map
void printMap(mapPoint map[][MAXMAPBOUNDS], int* row, int* colnum) {
	for (int i = 0; i < *row; i++) {
		for (int j = 0; j < *colnum; j++) {
			printf("%s ", map[i][j].value);
		}
		printf("\n");
	}
}

//Checks to see if a value in a given 2D array is NULL
int checkIfNull(int offSets[][VALUESIZE], int dir) {
	if(offSets[dir][0] == NONE) {
		return 1;
	}
	return 0;
}	

//Returns the pointer to a mapPoint specified by the direction
//represented by an int
//0 for up, 1 for down, 2 for left, 3 for right.
//Returns NULL for anything else.	
mapPoint* intRepresentationOfDirection(mapPoint* point, int dir) {
	if(dir == 0) {
		return point->up;
	}
	else if(dir == 1) {
		return point->down;
	}
	else if(dir == 2) {
		return point->left;
	}
	else if(dir == 3) {
		return point->right;
	}
	else {
		return NULL;
	}
}



//Assign pointers to other map points for a mapPoint	
void apointPointers(mapPoint map[][MAXMAPBOUNDS], mapPoint* point, int offSets[4][2]) {
		
	for(int trackdir = 0; trackdir < 4; trackdir++) {
		if(trackdir == 0) {
			if(checkIfNull(offSets, trackdir)) {
				point->up = NULL;
			}
			else {
				point->up = &map[point->row + offSets[trackdir][0]]
				[point->colnum + offSets[trackdir][1]];
			}
		}
		else if(trackdir == 1) {
			if(checkIfNull(offSets, trackdir)) {
				point->down = NULL;
			}
			else {
				point->down = &map[point->row + offSets[trackdir][0]]
				[point->colnum + offSets[trackdir][1]];
			}
		}
		else if(trackdir == 2) {
			if(checkIfNull(offSets, trackdir)) {
				point->left = NULL;
			}
			else {
				point->left = &map[point->row + offSets[trackdir][0]]
				[point->colnum + offSets[trackdir][1]];
			}
		}
		else if(trackdir == 3) {
			if(checkIfNull(offSets, trackdir)) {
				point->right = NULL;
			}
			else {
				point->right = &map[point->row + offSets[trackdir][0]]
				[point->colnum + offSets[trackdir][1]];
			}
		}
		else{
			point = NULL;
		}
	}
}


//Generate 2D array of offsets
void generateDirectionOffSets (mapPoint map[][MAXMAPBOUNDS],
 mapPoint* point, int upRow, int upCol, int downRow, int downCol, 
 int leftRow, int leftCol, int rightRow, int rightCol) {
	 
	 int offSets[4][2] = {{upRow, upCol}, {downRow, downCol}, 
		 {leftRow, leftCol}, {rightRow, rightCol}};
		 
	apointPointers(map, point, offSets);
 }


//Elements in the map are linked to one another.
//Each element has neighours up, down, left and right.
//These are assigned as pointers here.
void linkElementsInMap (mapPoint map[][MAXMAPBOUNDS], int * row, int* colnum) {
	
	for(int i = 0; i < *row; i++) {
		for(int j = 0; j < *colnum; j++) {
			mapPoint* point = &map[i][j];

			if(point->row == 0) {
				if(point->colnum == 0) {	
					generateDirectionOffSets(map, point, 999, 999,
					 1, 0, 999, 999, 0, 1);
				}
				else if (point->colnum == *colnum - 1) {
					generateDirectionOffSets(map, point, 999, 999,
					 1, 0, 0, -1, 999, 999);		
				}
				else {
					generateDirectionOffSets(map, point, 999, 999,
					 1, 0, 0, -1, 0, 1);
				}
			}
			else if(point->row == *row - 1) {
				if(point->colnum == 0) {
					generateDirectionOffSets(map, point, -1, 0,
					 999, 999, 999, 999, 0, 1);
				}
				else if (point->colnum == *colnum - 1) {
					generateDirectionOffSets(map, point, -1, 0,
					 999, 999, 0, -1, 999, 999);
				}
				else {
					point->up = &map[point->row - 1][point->colnum];
					point->down = NULL;
					point->left = &map[point->row][point->colnum - 1];
					point->right = &map[point->row][point->colnum + 1];
				}
			}
			else {
				if (point->colnum == 0) {
					point->up = &map[point->row - 1][point->colnum];
					point->down= &map[point->row + 1][point->colnum];
					point->left = NULL;
					point->right = &map[point->row][point->colnum + 1];
				}
				else if (point->colnum == *colnum - 1) {
					point->up = &map[point->row - 1][point->colnum];
					point->down = &map[point->row + 1][point->colnum];
					point->left = &map[point->row][point->colnum - 1];
					point->right = NULL;
				}
				else {
					point->up = &map[point->row - 1][point->colnum];
					point->down = &map[point->row + 1][point->colnum];
					point->left = &map[point->row][point->colnum - 1];
					point->right = &map[point->row][point->colnum + 1];
				}
			}
		}
	}
}


//Adds a point to the map.
void addPoint(int row, int colnum, char* text, mapPoint map[][MAXMAPBOUNDS]) {
		mapPoint* point = NULL;
		point = &map[row][colnum];
		point->row = row;
		point->colnum = colnum;
		strncpy(point->value, text, VALUESIZE - 1);
		point->value[VALUESIZE - 1] = '\0';
		point->visited = 0;
}		
			

//Consumes the map from stdin one character at a time using fgets.
//A MapPoint struct is then constructed and add to the Map 2D array.
void getMap(mapPoint map[][MAXMAPBOUNDS], int* row, int* colnum) {
	
	int currentRowToFill = 0;
	int currentColnumToFill = 0;
	
	char buffer[BUFFERSIZE] = {'\0'};
	while(fgets(buffer, BUFFERSIZE, stdin) != NULL) {
		if (buffer[0] == '\n') {
			continue;
		}
		if (currentRowToFill == *row) {
			break;
		}
		addPoint(currentRowToFill, currentColnumToFill, buffer, map);
		currentColnumToFill++;		
		if (currentColnumToFill == *colnum){
			currentRowToFill++;
			currentColnumToFill = 0;
		}
	}	
}


//Find the Emerald City in the map.
mapPoint* findE(mapPoint map[][MAXMAPBOUNDS], int* row, int* colnum) {
	
	mapPoint* point = NULL;
	for(int i = 0; i < *row; i++) {
		for(int j = 0; j < *colnum; j++) {
			point = &map[i][j];
			if(point->value[0] == 'E') {
				point->visited = 1;
				return point;
			}
		}
	}
	return point;
}


//Checks to see if Node is a valid part of the Yellow brick road.
int checkMove(mapPoint* node) {
	if(node == NULL) {
		return 0;
	}
	if(node->visited == 0) {
		if((node->value[0] =='Y') || (node->value[0] == 'D')) {
			return 1;
		}
	}
	return 0;
}


//Searches for dorathy by checking the nodes around it.
//Check to see if the surrounding nodes have already been visited
//and they are not Null pointers.
//Otherwise moves down the yellow brick road.
//
int findDorathy(mapPoint* emeraldCity) {
	mapPoint* currentNode = NULL;
	currentNode = emeraldCity;
	int count = 0;
	while(currentNode->value[0] != 'D') {
		if(checkMove(currentNode->up)){
			currentNode->visited = 1;
			currentNode = currentNode->up;
			count++;
		}
		else if(checkMove(currentNode->down)){
			currentNode->visited = 1;
			currentNode = currentNode->down;
			count++;
		}
		else if(checkMove(currentNode->left)){
			currentNode->visited = 1;
			currentNode = currentNode->left;
			count++;
		}
		else if(checkMove(currentNode->right)){
			currentNode->visited = 1;
			currentNode = currentNode->right;
			count++;
		}
		else{
			currentNode->visited = 1;
			return count;
		}
			 
	}
	
	return count;
}


int main(void) {	
	int mapBoundsRow = 0;
	int mapBoundsColnum = 0;
	
	mapPoint map[MAXMAPBOUNDS][MAXMAPBOUNDS];
	
	mapPoint* emeraldCity = NULL;
	
	int count;
	
	getRowsColnums(&mapBoundsRow, &mapBoundsColnum);
	
	getMap(map, &mapBoundsRow, &mapBoundsColnum);
	
	linkElementsInMap(map, &mapBoundsRow, &mapBoundsColnum);
	
	emeraldCity = findE(map, &mapBoundsRow, &mapBoundsColnum);
	
	count = findDorathy(emeraldCity);
	
	printMap(map, &mapBoundsRow, &mapBoundsColnum);
	
	printf("%d\n", count);
	
	return 0;
}

