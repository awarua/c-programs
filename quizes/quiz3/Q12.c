/* Variant of average.c in which the function is separated into
 * declaration and definition. The declaration must precede any calls
 * to the function.
 * Taken from the text by King, section 9.2, with slight modifications.
 */

#include <stdio.h>
#include <stdlib.h>

int isWonRow(char player, char game[3][3], int rowNum) {
	for (int i = 0; i < 3; i++) {
		if (!(player == game[rowNum][i])) {
			return(0);
		}
	}
	return(1);
}


int main() {
	char game[3][3] = {{'X', 'O', ' '},{'X', 'X', 'X'}, {' ', ' ', ' '}};
	printf("%d\n", isWonRow('X', game, 1));
	char game1[3][3] = {{'X', 'O', ' '},{' ', ' ', ' '}, {'X', 'X', 'O'}};
	printf("%d\n", isWonRow('X', game1, 2)); 

    return EXIT_SUCCESS;
}
