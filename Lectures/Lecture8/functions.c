#include <stdio.h>

#include "discriminate.h"

int main(void) {
	double a = 0;
	double b = 0;
	double c = 0;
	
	scanf("%lf %lf %lf", &a, &b, &c);
	printf("%lf\n", discriminate(a, b, c));
	return(0);
}
