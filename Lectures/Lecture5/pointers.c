#include <stdio.h>
#include <stdlib.h>

void printPointerAt(int* pThing) {
	printf("pThing points at the value %d\n", *pThing);
}

void printdata(int* data2) {
	printf("Size of the test data = %ld\n", sizeof(data2) / sizeof(int));
}

int main(void){
	int i = 10;
	int j = 20;
	int data[] = {10, 20, 30, 10000, -1, 0, -10000};
	printf("Size of the test data = %ld\n", sizeof(data) / sizeof(int));
	printdata(data);
	int* p = NULL;
	p = &i;
	//*p = j;
	printf("p = %p 1 = %d j = %d\n", p, i, j);
	printPointerAt(p);
	printPointerAt(&j);
	
	return(EXIT_SUCCESS);
}
