/* Variant of average.c in which the function is separated into
 * declaration and definition. The declaration must precede any calls
 * to the function.
 * Taken from the text by King, section 9.2, with slight modifications.
 */

#include <stdio.h>
#include <stdlib.h>

double discriminant(double a, double b, double c) {
 return ((b*b) - (4 * a * c));	
}


int main() {
    printf("%.2lf\n", discriminant(1, 2, 3));
    printf("%.2lf\n", discriminant(1.5, 1.5, 1.5));

    return EXIT_SUCCESS;
}
