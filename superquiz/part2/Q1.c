/*
 * Takes a char and finds the first digit (0-9) in char array.
 * Returns the address of the first digit.
 * If no digit is found then returns NULL.
 * 
 * Author: Dion Woolley
 * 
 * Date: 08/08/2014
 */


#include <stdio.h>

//Returns the address of the first digit in a char array.
//If no digits are found then returns NULL.
const char* findFirstDigit(const char* s) {
	
	int i = 0;
	
	while(s[i] != 0) {
		if (s[i] >= '0' && s[i] <= '9') {
			return(&s[i]);
		}
		i++;
	}
	return(NULL);
}


int main(void)
{
	const char* s = "I'm 91 years old";
	const char *address = findFirstDigit(s);
	printf("%p\n", address);
	printf("%p\n", s);
	
	const char* s1 = "I'm 91 years old";
	printf("%d\n", (int) (findFirstDigit(s1) - s1));
	
	const char* s2 = "Whee! This is great fun. I like this course.\n";
	const char* firstDigit = findFirstDigit(s2);
	printf("%s\n", firstDigit == NULL ? "NULL" : "Not NULL");	
	
	return 0;
}

