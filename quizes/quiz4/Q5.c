#include <stdio.h>
#include <stdlib.h>

void swap(int* p, int* q) {
	int temp = *p;
	*p = *q;
	*q = temp;
}

int main() {
	int a = 1;
	int b = 2;
	printf("Before swap %d, %d\n", a, b);
	swap(&a, &b);
	printf("After swap %d, %d\n", a, b);
	return(EXIT_SUCCESS);
}
